#!/data/venv/bin/python3.5
from slugify import slugify
import sys
import os

from zyxxid.character import Character, PDF
from zyxxid.spell import Spell

output_dir = "/data/spellcards"

character = Character()
character.name = "Spellbook"
character.classes = []
character.spells = []

if len(sys.argv) > 1:
    spells = []
    for spell_name in sys.argv[1:]:
        for spell_id in Spell.query("title", spell_name):
            spells.append(Spell.fetch(spell_id))

else:
    spells = Spell.all()

for spell in sorted(spells, key=lambda x: x.title):
    character.spells = [spell.id]

    try:
        pdf_id = PDF.create(character, "spellcards")
    except Exception as e:
        print("Could not generate PDF for spell {}: {}".format(spell.title, e))
        continue

    pdf = PDF.fetch(pdf_id)

    with open(os.path.join(output_dir, slugify(spell.title) + ".pdf"), "wb") as fh:
        fh.write(pdf.contents)

    print("Generated PDF for spell: {}".format(spell.title))
