#!/data/venv/bin/python3.5
import inspect
import os
import pkgutil
import sys

import zyxxid
from zyxxid.database import RiakStorable, RiakStorableFile

backup_dir = sys.argv[1]
if not os.path.exists(backup_dir):
    os.mkdir(backup_dir)

for importer, modname, ispkg in pkgutil.iter_modules(zyxxid.__path__):
    fullname = "zyxxid." + modname
    module = importer.find_module(fullname).load_module(fullname)

    for name, obj in inspect.getmembers(module):
        if inspect.isclass(obj) and obj != RiakStorable and issubclass(obj, RiakStorable) and not issubclass(obj, RiakStorableFile):
            filename = os.path.join(backup_dir, name.lower() + ".yaml.gz")
            print("Backing up {} to {}...".format(obj.__name__, filename))
            obj.export_all(filename)
