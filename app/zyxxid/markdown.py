import mistune


class LatexRenderer(mistune.Renderer):
    def enclose(self, tag, contents):
        return "\n\\begin{{{tag}}}\n{contents}\n\\end{{{tag}}}\n".format(tag=tag, contents=contents)

    def block_code(self, code, lang):
        return self.enclose("verbatim", code)

    def block_quote(self, text):
        return self.enclose("quotation", text)

    def header(self, text, level, raw=None):
        return "\{}section{{{}}}\n".format("sub" * (level - 1), text)

    def list(self, body, ordered=True):
        return self.enclose("itemize", body)

    def list_item(self, text):
        return "\\item " + text + "\n"

    def link(self, link, title, text):
        return text

    def paragraph(self, text):
        return text + "\n\n"

    def linebreak(self):
        return "\n"

    def emphasis(self, text):
        return "\\textit{{{}}}".format(text)

    def double_emphasis(self, text):
        return "\\textbf{{{}}}".format(text)

    def codespan(self, text):
        return "\\texttt{{{}}}".format(text)

    def hrule(self):
        return "\\hrulefill"

    def footnotes(self, text):
        print(text)
        return text

    def footnote_ref(self, key, index):
        print(key, index)
        return "\\footnotemark[%s]".format(key)

    def footnote_item(self, key, text):
        return "\\footnotetext[{}]{{{}}}".format(key, text)

    def reference(self, key):
        return "\\cite{{{}}}".format(key)

    def image(self, src, title, text):
        return "\n".join([
            "\\begin{figure}",
            "\\includegraphics{{{}}}".format(src),
            "\\caption{{{}}}".format(title),
            "\\label{{{}}}".format(text),
            "\\end{figure}",
        ])
