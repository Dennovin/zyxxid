#!/bin/bash
service nginx stop
letsencrypt renew
cp /etc/letsencrypt/live/zyxxid.xyz/* /data/certs/
service nginx start
