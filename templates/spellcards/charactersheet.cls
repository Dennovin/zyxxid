\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{charactersheet} [2015/06/01 D&D 5e Character Sheet]

\LoadClass{article}

\usepackage{fontspec,tabularx,tabulary,fp,numprint,etoolbox,enumitem,cellspace,color,colortbl,titlesec,adjustbox,forloop,amssymb,setspace,multicol,multido}
\usepackage[margin=0.25in,papersize={5in,3in}]{geometry}
\usepackage[table]{xcolor}

\setmainfont[Scale=0.90]{PT Sans}
\newfontfamily\titlett{Aniron}
\newfontfamily\sectiontt[UprightFont={* Bold}]{PT Sans}
\newfontfamily\spelldesctt[Scale=0.75]{PT Sans}
\titleformat{\section}{\titlett}{}{}{}{}

\pagestyle{empty}
\tymin=1.5in

\addparagraphcolumntypes{X}
\cellspacetoplimit 5pt
\cellspacebottomlimit 3pt
\setlength{\parindent}{0pt}
\setlength{\columnsep}{0.5cm}

\definecolor{gray}{rgb}{0.75,0.75,0.75}
\definecolor{yellow}{rgb}{1.00,1.00,0.75}
\definecolor{oddrow}{rgb}{0.90,0.90,1.00}
\definecolor{evenrow}{rgb}{0.80,0.80,0.90}
\definecolor{spellcolor0}{rgb}{0.90,0.90,0.90}
\definecolor{spellcolor1}{rgb}{1.00,0.60,0.60}
\definecolor{spellcolor2}{rgb}{1.00,0.80,0.60}
\definecolor{spellcolor3}{rgb}{0.90,0.90,0.60}
\definecolor{spellcolor4}{rgb}{0.70,1.00,0.60}
\definecolor{spellcolor5}{rgb}{0.60,1.00,0.80}
\definecolor{spellcolor6}{rgb}{0.60,0.90,0.90}
\definecolor{spellcolor7}{rgb}{0.60,0.70,1.00}
\definecolor{spellcolor8}{rgb}{0.70,0.60,1.00}
\definecolor{spellcolor9}{rgb}{0.90,0.60,0.90}

\newenvironment{charactersheet}
  {
    \newcommand{\spells}{}
    \newcommand{\addspell}[9]{\expandafter\def\expandafter\spells\expandafter{\spells{}
        \begin{tabularx}{\linewidth}{ |X Sc| }
          \hline
          \rowcolor{spellcolor##4} \textbf{##1} & \hspace*{\fill} {\spelldesctt \setstretch{0.7} ##3} \\
          \hline
        \end{tabularx}

        \par\vspace{0.5em}
        \par\noindent{\spelldesctt \setstretch{0.7} \ifthenelse{\equal{##2}{True}}{\textit{Ritual} $\cdot$ }{} \textit{##7} $\cdot$ \textit{##5}} \\
        \par\vspace{-0.5em}
        \par\noindent{\spelldesctt \setstretch{0.7} ##9 \par}
        \par\vspace*{\fill}
        \par\noindent{\spelldesctt \setstretch{0.7} \textit{Range: ##6} $\cdot$ \textit{Duration: ##8}} \\
        \par
        \pagebreak
    }}
  }
  {
    \spells
  }
